# Extract Text From Files Wiki

## About this endpoint
This endpoint is useful for extracting text from various document types, images and audio files for further analysis. Later on, you may choose to find important keywords or try to recognize entities from the extracted text, the applications are endless. This endpoint acts as an initial step for further text analysis.

## Getting started
**Python code**
```python
# make sure you have python3 and pip3 installed on your computer
# make sure you have requests library installed (pip3 install requests)
# get your api key from the rapidapi console

import requests


api_key = '*** insert your api key here ***'
filename = '*** full path of your document file here ***'  # can be any supported file like pdf, csv etc
url = 'https://text-analysis12.p.rapidapi.com/text-mining/api/v1.1'

headers = {
	'x-rapidapi-key': api_key,
	'x-rapidapi-host': 'text-analysis12.p.rapidapi.com'
}

files = {
	'input_file': open(filename, 'rb')
}

response = requests.post(url=url, headers=headers, files=files)
result = response.json()

if result['ok']:
	text = result['text']
	print(f'text from {filename} is:')
	print(text)
	# do_something_with_text(text)
else:
	print(f'error: {result["msg"]}')
```

## Languages supported

**full-code, short-code**
- english, en

**You can use either full-code or short-code for selecting the language**

## File extensions supported
- csv
- doc
- docx
- eml
- epub
- json
- html
- htm
- msg
- odt
- pdf
- pptx
- ps
- rtf
- txt
- xls
- xlsx
- gif
- jpg
- jpeg
- png
- tiff
- tif
- mp3
- ogg
- wav

## Service Limits

- max file input size: 10MB
- max audio length: 60s (you can split audio files into smaller parts using FFmpeg)
- rate limit: 100 requests per second
 